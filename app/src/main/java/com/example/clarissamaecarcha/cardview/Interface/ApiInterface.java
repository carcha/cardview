package com.example.clarissamaecarcha.cardview.Interface;

import com.example.clarissamaecarcha.cardview.Model.Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Clarissa Mae Carcha on 08/09/2017.
 */

public interface ApiInterface {

    @GET("data.json")
    Call<List<Model>> getArticles();
}
