package com.example.clarissamaecarcha.cardview.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.clarissamaecarcha.cardview.Adapter.MyAdapter;
import com.example.clarissamaecarcha.cardview.Interface.ApiInterface;
import com.example.clarissamaecarcha.cardview.Model.Model;
import com.example.clarissamaecarcha.cardview.R;
import com.example.clarissamaecarcha.cardview.Retrofit.RetrofitApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ApiInterface apiInterface;
    private ProgressBar progressBar;
    private TextView textView;
    private RecyclerView recyclerView;

    private MyAdapter articleReadingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiInterface = RetrofitApiClient.getClient().create(ApiInterface.class);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textView = (TextView) findViewById(R.id.textView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewArticleReading);

        if(isNetworkAvailable())
            callToServer();
        else{
            progressBar.setVisibility(View.GONE);
            textView.setText("No internet Connection available!");
        }


    }

    private void callToServer() {

        Call<List<Model>> call = apiInterface.getArticles();
        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                progressBar.setVisibility(View.GONE);
                List<Model> articleModelList = response.body();

                //Show Article List
                try {
                    if(articleModelList.size()>0){
                        articleReadingAdapter = new MyAdapter(getApplicationContext(), articleModelList, recyclerView);
                        recyclerView.setAdapter(articleReadingAdapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayout.VERTICAL, false));
                    }
                    else
                        textView.setText("Article List is Empty!");
                }
                catch (Exception e){
                    textView.setText(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                textView.setText(t.getMessage());
            }
        });

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
